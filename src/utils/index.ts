/*
 * @Description:
 * @Author: Senkita
 * @Date: 2021-06-02 17:46:47
 * @LastEditors: Senkita
 * @LastEditTime: 2021-06-10 00:00:47
 */
interface ITodos {
    id: number;
    text: string;
    completed: boolean;
}

interface IPayload {
    id: number;
    newVal: string;
}

export const getTodos = (): ITodos[] => {
    return JSON.parse(localStorage.getItem('todos') || '[]');
};

export const setTodos = (todos: ITodos[]) => {
    localStorage.setItem('todos', JSON.stringify(todos));
};

export const endEditTodo = (store, payload: IPayload) =>
    store.dispatch('endEdit', payload);
