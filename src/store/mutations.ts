/*
 * @Description:
 * @Author: Senkita
 * @Date: 2021-06-09 15:34:14
 * @LastEditors: Senkita
 * @LastEditTime: 2021-06-09 23:40:50
 */

import { setTodos } from '../utils';

interface ITodos {
    id: number;
    text: string;
    completed: boolean;
    edit: boolean;
}

interface IPayload {
    id: number;
    newVal: string;
}

const mutations = {
    add(state, newTodo: string) {
        state.todos.unshift({
            id: new Date().getTime(),
            text: newTodo,
            completed: false,
            edit: false,
        });

        setTodos(state.todos);
    },
    delete(state, id: number) {
        state.todos.splice(
            state.todos.findIndex(
                (item: ITodos) => {
                    return item.id === id;
                },
                { id }
            ),
            1
        );
        setTodos(state.todos);
    },
    edit(state, id: number) {
        const index = state.todos.findIndex(
            (item: ITodos) => {
                return item.id === id;
            },
            { id }
        );
        state.todos[index].edit = true;
    },
    endEdit(state, payload: IPayload) {
        const index = state.todos.findIndex((item: ITodos) => {
            return item.id === payload.id;
        }, payload.id);

        state.todos[index].text = payload.newVal;
        state.todos[index].edit = false;

        setTodos(state.todos);
    },
};
export default mutations;
