/*
 * @Description:
 * @Author: Senkita
 * @Date: 2021-06-09 15:33:53
 * @LastEditors: Senkita
 * @LastEditTime: 2021-06-09 23:38:14
 */
const actions = {
    create: ({ commit }, newTodo: string) => {
        if (newTodo) commit('add', newTodo);
    },
    remove: ({ commit }, id: number) => commit('delete', id),
    edit: ({ commit }, id: number) => commit('edit', id),
    endEdit: ({ commit }, payload) => commit('endEdit', payload),
};
export default actions;
