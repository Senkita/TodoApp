/*
 * @Description:
 * @Author: Senkita
 * @Date: 2021-06-09 15:33:59
 * @LastEditors: Senkita
 * @LastEditTime: 2021-06-09 15:48:36
 */
import { getTodos } from '../utils';

const state = {
    todos: getTodos(),
};
export default state;
