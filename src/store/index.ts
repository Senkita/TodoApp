/*
 * @Description:
 * @Author: Senkita
 * @Date: 2021-05-31 18:27:17
 * @LastEditors: Senkita
 * @LastEditTime: 2021-06-09 16:48:00
 */
import { createStore } from 'vuex';
import actions from './actions';
import state from './state';
import mutations from './mutations';

const store = createStore({
    actions,
    state,
    mutations,
});

export default store;
