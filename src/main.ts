/*
 * @Description:
 * @Author: Senkita
 * @Date: 2021-05-26 22:22:56
 * @LastEditors: Senkita
 * @LastEditTime: 2021-06-09 15:44:10
 */
import { createApp } from 'vue';
import '@assets/styles/reset.css';
import '@assets/styles/index.css';
import App from '@src/App.vue';
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import store from './store';
import router from './routes';

const app = createApp(App);
// @ts-ignore
app.config.productionTip = false;
app.use(Antd).use(router).use(store).mount('#app');
