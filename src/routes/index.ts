/*
 * @Description:
 * @Author: Senkita
 * @Date: 2021-06-09 15:40:38
 * @LastEditors: Senkita
 * @LastEditTime: 2021-06-09 15:44:00
 */
import { createRouter, createWebHashHistory } from 'vue-router';
import TodoApp from '@pages/TodoApp.vue';

const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: '/',
            component: TodoApp,
        },
    ],
});

export default router;
