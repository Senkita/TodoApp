/*
 * @Description:
 * @Author: Senkita
 * @Date: 2021-05-26 22:22:56
 * @LastEditors: Senkita
 * @LastEditTime: 2021-06-09 15:39:26
 */
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import { resolve } from 'path';

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [vue()],
    resolve: {
        alias: {
            '@src': resolve(__dirname, 'src'),
            '@pages': resolve(__dirname, 'src/pages'),
            '@assets': resolve(__dirname, 'src/assets'),
            '@components': resolve(__dirname, 'src/components'),
            '@tests': resolve(__dirname, 'tests'),
        },
    },
});
