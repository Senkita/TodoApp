/*
 * @Description:
 * @Author: Senkita
 * @Date: 2021-05-27 12:35:38
 * @LastEditors: Senkita
 * @LastEditTime: 2021-05-27 12:46:04
 */
module.exports = {
    purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {},
    },
    variants: {
        extend: {},
    },
    plugins: [],
};
