/*
 * @Description:
 * @Author: Senkita
 * @Date: 2021-05-28 21:53:02
 * @LastEditors: Senkita
 * @LastEditTime: 2021-05-28 21:53:03
 */
Object.defineProperty(window, 'matchMedia', {
    writable: true,
    value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // deprecated
        removeListener: jest.fn(), // deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
    })),
});
