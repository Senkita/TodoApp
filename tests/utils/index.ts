/*
 * @Description:
 * @Author: Senkita
 * @Date: 2021-05-27 12:07:07
 * @LastEditors: Senkita
 * @LastEditTime: 2021-05-27 18:15:27
 */
import { VueWrapper, DOMWrapper } from '@vue/test-utils';

export const getWrapper = (
    wrapper: VueWrapper<any>,
    name: string
): Omit<DOMWrapper<Element>, 'exists'> => {
    return wrapper.get(`[data-test='${name}']`);
};

export const findAllWrapper = (
    wrapper: VueWrapper<any>,
    name: string
): DOMWrapper<Element>[] => {
    return wrapper.findAll(`[data-test='${name}']`);
};

export const findComponent = (
    wrapper: VueWrapper<any>,
    name: string
): VueWrapper<any> => {
    return wrapper.findComponent(`[data-test=${name}]`);
};

export const findAllComponents = (
    wrapper: VueWrapper<any>,
    name: string
): VueWrapper<any>[] => {
    return wrapper.findAllComponents(`[data-test=${name}]`);
};
