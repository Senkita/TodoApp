/*
 * @Description:
 * @Author: Senkita
 * @Date: 2021-05-27 02:58:33
 * @LastEditors: Senkita
 * @LastEditTime: 2021-05-28 17:12:10
 */
import { shallowMount } from '@vue/test-utils';

// 测试组件
const MessageComponent = {
    template: '<p>{{ msg }}</p>',
    props: ['msg'],
};

test('show msg', () => {
    const wrapper = shallowMount(MessageComponent, {
        props: {
            msg: 'Hello world',
        },
    });

    // 断言组件生成文本
    expect(wrapper.text()).toContain('Hello world');
});
