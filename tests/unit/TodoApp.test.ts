/*
 * @Description:
 * @Author: Senkita
 * @Date: 2021-05-27 03:13:11
 * @LastEditors: Senkita
 * @LastEditTime: 2021-06-09 15:38:36
 */
import { mount, VueWrapper } from '@vue/test-utils';
// @ts-ignore
import TodoApp from '@pages/TodoApp.vue';
// @ts-ignore
import { getWrapper, findAllWrapper } from '@tests/utils';
import Antd from 'ant-design-vue';

describe('TodoApp.vue', () => {
    const wrapper: VueWrapper<any> = mount(TodoApp, {
        global: {
            plugins: [Antd],
        },
    });

    it('mount a todo', () => {
        expect(getWrapper(wrapper, 'todo').text()).toBe('Learn Vue.js 3');
    });

    it('create a new todo', async () => {
        expect(findAllWrapper(wrapper, 'todo')).toHaveLength(1);

        await getWrapper(wrapper, 'new-todo').setValue('New todo');
        await getWrapper(wrapper, 'form').trigger('submit');

        expect(findAllWrapper(wrapper, 'todo')).toHaveLength(2);
    });

    it('complete a todo', async () => {
        await getWrapper(wrapper, 'todo-checkbox').setValue(true);

        expect(getWrapper(wrapper, 'todo').classes()).toContain('completed');
    });
});
