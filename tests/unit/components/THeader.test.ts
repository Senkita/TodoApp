/*
 * @Description:
 * @Author: Senkita
 * @Date: 2021-05-27 18:17:12
 * @LastEditors: Senkita
 * @LastEditTime: 2021-05-31 17:32:14
 */
import { VueWrapper, mount } from '@vue/test-utils';
// @ts-ignore
import THeader from '@components/THeader.vue';
// @ts-ignore
import { findComponent } from '@tests/utils';
import Antd from 'ant-design-vue';

describe('THeader.vue', () => {
    let wrapper: VueWrapper<any>;
    let createTodo: jest.Mock;
    beforeAll(() => {
        createTodo = jest.fn();

        wrapper = mount(THeader, {
            global: {
                plugins: [Antd],
                mocks: {
                    createTodo,
                    search: createTodo,
                },
            },
        });
    });

    it('form submit', async () => {
        expect(createTodo).toHaveBeenCalledTimes(0);
        await findComponent(wrapper, 'form').trigger('submit');
        expect(createTodo).toHaveBeenCalledTimes(1);
    });

    it('input click', async () => {
        expect(createTodo).toHaveBeenCalledTimes(0);
        await wrapper.get('.ant-input-search-button').trigger('click');
        expect(createTodo).toHaveBeenCalledTimes(1);
    });
});
