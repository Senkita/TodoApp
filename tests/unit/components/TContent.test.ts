/*
 * @Description:
 * @Author: Senkita
 * @Date: 2021-05-28 19:41:11
 * @LastEditors: Senkita
 * @LastEditTime: 2021-05-31 16:47:46
 */
import { mount, VueWrapper } from '@vue/test-utils';
import Antd from 'ant-design-vue';
// @ts-ignore
import TContent from '@components/TContent.vue';
// @ts-ignore
import { getWrapper, findComponent } from '@tests/utils';

interface ITodos {
    id: number;
    text: string;
    completed: boolean;
}

describe('TContent.vue', () => {
    const todos: ITodos[] = [
        {
            id: 1,
            text: '测试',
            completed: false,
        },
    ];
    const deleteTodo: jest.Mock = jest.fn();
    const editTodo: jest.Mock = jest.fn();

    const wrapper: VueWrapper<any> = mount(TContent, {
        props: {
            todos,
        },
        global: {
            plugins: [Antd],
            mocks: {
                deleteTodo,
                editTodo,
            },
        },
    });

    it('mount', () => {
        expect(findComponent(wrapper, 'todo').text()).toContain('测试');
    });

    it('complete a todo', async () => {
        expect(findComponent(wrapper, 'todo').classes()).not.toContain(
            'completed'
        );
        await wrapper.get('.ant-checkbox-input').setValue(true);
        expect(findComponent(wrapper, 'todo').classes()).toContain('completed');
    });

    it('delete a todo', async () => {
        expect(deleteTodo).toBeCalledTimes(0);
        await getWrapper(wrapper, 'deleteTodo').trigger('click');
        expect(deleteTodo).toBeCalledTimes(1);
    });

    it('edit a todo', async () => {
        await getWrapper(wrapper, 'editTodo').trigger('click');
        expect(editTodo).toBeCalledTimes(1);
    });
});
