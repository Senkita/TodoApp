/*
 * @Description:
 * @Author: Senkita
 * @Date: 2021-05-27 17:50:24
 * @LastEditors: Senkita
 * @LastEditTime: 2021-05-28 17:15:55
 */
import { VueWrapper, mount } from '@vue/test-utils';
// @ts-ignore
import TFooter from '@components/TFooter.vue';
// @ts-ignore
import { findComponent } from '@tests/utils';
import Antd from 'ant-design-vue';

describe('TFooter.vue', () => {
    it('mount TFooter', () => {
        const wrapper: VueWrapper<any> = mount(TFooter, {
            global: {
                plugins: [Antd],
            },
        });
        const year: number = new Date().getFullYear();

        expect(findComponent(wrapper, 'copyright').text()).toBe(
            `Copyright © ${year} Senkita`
        );
    });
});
